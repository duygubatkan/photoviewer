package viewer.photo.flickr.com.photoviewer;

/**
 * Created by apple on 9.03.2017.
 */

public class Photograph {
    private int farmid;
    private String serverid;
    private String id;
    private String secret;


    public Photograph(int FarmID, String ServerID, String ID, String Secret){
        setFarmid(FarmID);
        setServerid(ServerID);
        setId(ID);
        setSecret(Secret);
    }

    public int getFarmid() {
        return farmid;
    }

    public void setFarmid(int farmid) {
        this.farmid = farmid;
    }

    public String getServerid() {
        return serverid;
    }

    public void setServerid(String serverid) {
        this.serverid = serverid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

}
