package viewer.photo.flickr.com.photoviewer.Retrofit;


import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by apple on 8.03.2017.
 */

public interface RetroInterface {
    @GET("/?method=flickr.photos.getRecent")
     void getRecent(
            @Query("api_key") String apiKey,
            @Query("format") String format,
            @Query("nojsoncallback") int no,
            Callback<Photos> callback

    );

    @GET("/?method=flickr.photos.search")
    void search(
            @Query("api_key") String apiKey,
            @Query("text") String text,
            @Query("format") String format,
            @Query("nojsoncallback") int no,
            Callback<Photos> callback
    );

}
