package viewer.photo.flickr.com.photoviewer.Retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by apple on 9.03.2017.
 */

public class PhotosList {

    @SerializedName("photo")
    @Expose
    private List<PhotoList> photoList= new ArrayList<>();


    public List<PhotoList> getPhotoList() {
        return photoList;
    }

    public void setPhotoList(List<PhotoList> photoList) {
        this.photoList = photoList;
    }

}


