package viewer.photo.flickr.com.photoviewer.Retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by apple on 9.03.2017.
 */

public class Photos {
    @SerializedName("photos")
    @Expose
    private PhotosList photoslist;

    public PhotosList getPhotoslist() {
        return photoslist;
    }

    public void setPhotoslist(PhotosList photoslist) {
        this.photoslist = photoslist;
    }
}
