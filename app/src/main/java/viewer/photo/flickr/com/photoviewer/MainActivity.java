package viewer.photo.flickr.com.photoviewer;

import android.app.SearchManager;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import viewer.photo.flickr.com.photoviewer.Retrofit.Photos;
import viewer.photo.flickr.com.photoviewer.Retrofit.RetroInterface;


public class MainActivity extends AppCompatActivity {
    private Toolbar toolBar;
    private RecyclerView recyclerView;
    private GridLayoutManager gridLayout;
    List<Photograph> photographList = new ArrayList<Photograph>();
    String searchText;
    SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        //Toolbar settings
        toolBar = (Toolbar) findViewById(R.id.toolBar);
        toolBar.setTitle("Flicr");
        setSupportActionBar(toolBar);

        //retrofit API for random photo
        RestAdapter retrofit = new RestAdapter.Builder().setEndpoint(getString(R.string.baseurl)).build();
        RetroInterface retrofitAPI = retrofit.create(RetroInterface.class);
        retrofitAPI.getRecent("608f8d004396946b740e92753e7e8321", "json", 1, new Callback<Photos>() {
            @Override
            public void success(Photos photos, Response response) {
                photographList.clear();
                for (int i = 0; i < photos.getPhotoslist().getPhotoList().size(); i++) {
                    photographList.add(new Photograph(
                            photos.getPhotoslist().getPhotoList().get(i).getFarm(),
                            photos.getPhotoslist().getPhotoList().get(i).getServer(),
                            photos.getPhotoslist().getPhotoList().get(i).getId(),
                            photos.getPhotoslist().getPhotoList().get(i).getSecret()));
                }
                AdapterRecyclerView adaptor = new AdapterRecyclerView(MainActivity.this, photographList);
                recyclerView.setAdapter(adaptor);
                gridLayout = new GridLayoutManager(MainActivity.this, 2);
                recyclerView.setLayoutManager(gridLayout);
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_bar, menu);

        //searchview item settings
        MenuItem searchViewItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) searchViewItem.getActionView();
        searchView.setQueryHint("Search with Tags...");
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(true);
        SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
            public boolean onQueryTextChange(String newText) {
                return true;
            }

            public boolean onQueryTextSubmit(String query) {
                searchText = query;
                search(searchText);

                //Snackbar settings
                Snackbar snack = Snackbar.make(findViewById(R.id.activity_main), R.string.LoadingImfromation, Snackbar.LENGTH_LONG)
                        .setAction("Action", null);
                ViewGroup group = (ViewGroup) snack.getView();
                group.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.greyLight));
                snack.show();
                return true;
            }
        };
        searchView.setOnQueryTextListener(queryTextListener);
        return true;

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    //retrofit api for search text
    public void search(String text) {
        RestAdapter retrofit = new RestAdapter.Builder().setEndpoint(getString(R.string.baseurl)).build();
        RetroInterface retrofitAPI = retrofit.create(RetroInterface.class);
        retrofitAPI.search("608f8d004396946b740e92753e7e8321", text, "json", 1, new Callback<Photos>() {
            @Override
            public void success(Photos photos, Response response) {
                photographList.clear();
                for (int i = 0; i < photos.getPhotoslist().getPhotoList().size(); i++) {
                    photographList.add(new Photograph(
                            photos.getPhotoslist().getPhotoList().get(i).getFarm(),
                            photos.getPhotoslist().getPhotoList().get(i).getServer(),
                            photos.getPhotoslist().getPhotoList().get(i).getId(),
                            photos.getPhotoslist().getPhotoList().get(i).getSecret()));

                }

                AdapterRecyclerView adaptor = new AdapterRecyclerView(MainActivity.this, photographList);
                recyclerView.setAdapter(adaptor);
                recyclerView.setHasFixedSize(true);
                recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
                gridLayout = new GridLayoutManager(MainActivity.this, 2);
                recyclerView.setLayoutManager(gridLayout);
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }
}
