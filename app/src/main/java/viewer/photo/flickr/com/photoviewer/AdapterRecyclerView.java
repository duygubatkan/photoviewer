package viewer.photo.flickr.com.photoviewer;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by apple on 13.03.2017.
 */

public class AdapterRecyclerView extends RecyclerView.Adapter<AdapterRecyclerView.ViewHolder> {

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView photoViewImageView;

        public ViewHolder(View view) {
            super(view);
            photoViewImageView = (ImageView) view.findViewById(R.id.photoViewImageView);
        }
    }

    List<Photograph> photograph;
    Context context;
    Photograph list;

    public AdapterRecyclerView(Context context, List<Photograph> mPhotograph) {
        this.context = context;
        this.photograph = mPhotograph;
    }

    @Override
    public AdapterRecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        list = photograph.get(position);
        final String farmId = String.valueOf(photograph.get(position).getFarmid());
        final String serverId = photograph.get(position).getServerid();
        final String id = photograph.get(position).getId();
        final String secret = photograph.get(position).getSecret();

        String URL = "https://farm" + farmId + ".staticflickr.com/" + serverId + "/" + id + "_" + secret + ".jpg";
        Picasso.with(context).load(URL).into(holder.photoViewImageView);
        holder.photoViewImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list = photograph.get(position);
                Intent intent = new Intent(context, FullScreen.class);
                intent.putExtra("farmID", farmId);
                intent.putExtra("serverid", serverId);
                intent.putExtra("id", id);
                intent.putExtra("secret", secret);
                context.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return photograph.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

}