package viewer.photo.flickr.com.photoviewer;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

/**
 * Created by apple on 10.03.2017.
 */

public class FullScreen extends AppCompatActivity implements Animation.AnimationListener {
    Context context;
    Animation animation;
    ImageView fullScreenImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fullscreen);

        fullScreenImageView = (ImageView) findViewById(R.id.fullScreenImageView);
        animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.animator);
        animation.setAnimationListener(FullScreen.this);
        fullScreenImageView.startAnimation(animation);
        Intent intent = getIntent();
        String URL = R.string.httpsFarm + intent.getStringExtra("farmID") + R.string.staticflickr + intent.getStringExtra("serverid") + "/" + intent.getStringExtra("id") + "_" + intent.getStringExtra("secret") + ".jpg";
        Picasso.with(context).load(URL).into(fullScreenImageView);

    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
